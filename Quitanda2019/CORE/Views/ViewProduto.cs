﻿using CORE.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Views
{
   public class ViewProduto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Un { get; set; }
        public string Embalagem { get; set; }
        public Nullable<decimal> CustoUnitario { get; set; }
        public Nullable<decimal> PrecoVenda { get; set; }
        public Nullable<int> IDCategoria { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual string DescricaoCategoria
        {
            get
            {
                if(Categoria == null)
                {
                    throw new NullReferenceException("Categoria não incluída na carga");
                }
                return Categoria.Descricao;
            }
        }

    }
}
