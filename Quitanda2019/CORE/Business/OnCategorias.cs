﻿using CORE.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Business
{
    public class OnCategorias : IEntidade<Categoria>
    {
        public void Excluir(Categoria registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Entry(registro).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Categoria> Lista()
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    return db.Categorias.OrderBy(c => c.Descricao).ToList();


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Novo(Categoria registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Categorias.Add(registro);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Salvar(Categoria registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
