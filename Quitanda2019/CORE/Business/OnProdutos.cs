﻿using CORE.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CORE.Business
{
    public class OnProdutos : IEntidade<Produto>
    {
        public void Excluir(Produto registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Entry(registro).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Produto> Lista()
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    return db.Produtos.Include("Categoria").OrderBy(p => p.Descricao).ToList();


                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Novo(Produto registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Produtos.Add(registro);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Salvar(Produto registro)
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {

                    db.Entry(registro).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Views.ViewProduto> ListaProdutos()
        {
            try
            {
                using (var db = new Quitanda2019Entities())
                {
                    var dados = from p in db.Produtos.Include("Categoria")
                                orderby p.Descricao
                                select new Views.ViewProduto
                                {
                                    Id = p.Id,
                                    Descricao = p.Descricao,
                                    PrecoVenda = p.PrecoVenda,
                                    CustoUnitario = p.CustoUnitario,
                                    Un = p.Un,
                                    Embalagem = p.Embalagem,
                                    IDCategoria = p.IDCategoria,
                                    Categoria = p.Categoria
                                };
                    return dados.ToList();
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
