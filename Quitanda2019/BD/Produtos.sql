﻿CREATE TABLE [dbo].[Produtos]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Descricao] VARCHAR(50),
	[Un] VARCHAR (20),
	[Embalagem] VARCHAR (20),
	[CustoUnitario] MONEY,
	[PrecoVenda] MONEY,
	[IDCategoria] int, 
    CONSTRAINT [FK_Produtos_ToCategorias] FOREIGN KEY ([Id]) REFERENCES [Categorias]([Id])
)
