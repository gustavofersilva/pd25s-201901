﻿using CORE.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIWINDOWS2019_1
{
    public partial class FormProduto : Form
    {
        public FormProduto()
        {
            InitializeComponent();
            produtoBindingSource.DataSource = new CORE.Business.OnProdutos().ListaProdutos();
            var categorias = new OnCategorias().Lista();
            cmbCategoria.Items.Add("(Todas)");
            foreach (var c in categorias)
            {
                cmbCategoria.Items.Add(c.Descricao);
            }
            cmbCategoria.SelectedIndex = 0;
        }

        private void cmbCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCategoria.SelectedIndex == 0)
            {
                produtoBindingSource.DataSource = new OnProdutos().ListaProdutos();
            } else
            {
                produtoBindingSource.DataSource = new OnProdutos().ListaProdutos()
                                                                   .Where(p => p.DescricaoCategoria == cmbCategoria.SelectedItem.ToString());
            }
        }
    }


}
