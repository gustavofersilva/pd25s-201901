﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CORE.Business;

namespace GUIWINDOWS2019_1
{
    public partial class FormCategorias : Form
    {
        public FormCategorias()
        {
            InitializeComponent();
            categoriaBindingSource.DataSource = new OnCategorias().Lista();
        }


        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            var form = new FormCategoria();
            form.Novo();
            form.ShowDialog();
        }
    }
}
