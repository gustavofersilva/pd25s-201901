﻿using CORE.Business;
using CORE.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIWINDOWS2019_1
{
    
    public partial class FormCategoria : Form
    {
        public EnumOperacoes op { get; set; }

        public FormCategoria()
        {
            InitializeComponent();
        }

        public void Novo()
        {
            op = EnumOperacoes.Novo;
            categoriaBindingSource.AddNew();
        }

        private void Editar(Categoria c)

        {
            categoriaBindingSource.DataSource = c;
            op = EnumOperacoes.Editar;
        }

        private void Salvar()
        {
            categoriaBindingSource.EndEdit();
            var registro = categoriaBindingSource.Current as Categoria;
            try
            {
                if (op == EnumOperacoes.Novo)
                {

                    new OnCategorias().Novo(registro);
                }
                else
                {
                    registro = categoriaBindingSource.Current as Categoria;
                    new OnCategorias().Salvar(registro);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Falha ao salvar registro: " + ex.Message);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Salvar();
            Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var form = new FormCategoria();
            form.Editar(categoriaBindingSource.Current as Categoria);
            form.ShowDialog();
        }

        private void bindingNavigatorDeleteItem_click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja excluir o registro?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var c = categoriaBindingSource.Current as Categoria;
                new OnCategorias().Excluir(c);
                categoriaBindingSource.RemoveCurrent();
            }

        }
    }
}
