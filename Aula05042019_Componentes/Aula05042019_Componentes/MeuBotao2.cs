﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aula05042019_Componentes
{
    public partial class MeuBotao2 : Component
    {
        public MeuBotao2()
        {
            InitializeComponent();
        }

        public MeuBotao2(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
