﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeusComponentes
{
    public partial class MeuBotao : Control
    {
        public MeuBotao()
        {
            InitializeComponent();
        }
        private bool _Hovering = false;
        private bool Hovering
        {
            get { return _Hovering; }
            set
            {
                if (value == _Hovering) return;
                _Hovering = value;
                Invalidate(); // Força o redesenho do controle
            }
        }

        private bool _Pressed = false;
        private bool Pressed
        {
            get { return _Pressed; }
            set
            {
                if (value == _Pressed) return;
                _Pressed = value;
                Invalidate();
            }
        }

        public new String Text  // sobrepõe a propriedade da classe Control
        {
            get { return base.Text; }
            set
            {
                if (value == base.Text) return;
                base.Text = value;
                Invalidate();
            }
        }

        public enum ShapeType
        {
            Oval,
            Rectangle
        }

        private ShapeType _Shape;

        /// <summary>
        /// Modifica o formato do botão.
        /// </summary>

        [Description("Modifica o formato do botão."),
         Category("Appearance"),
         DefaultValue(typeof(ShapeType), "Oval"),
         Browsable(true)]
        public ShapeType Shape
        {
            get { return _Shape; }
            set
            {
                if (value == _Shape) return;
                _Shape = value;
                Invalidate();
            }
        }

        private float _BorderSize;

        /// <summary>
        /// Modifica o tamanho da borda
        /// </summary>

        [Description("Modifica o tamanho da borda."),
         Category("Appearance"),
         DefaultValue(typeof(float), "Borda"),
         Browsable(true)]
        public float BorderSize
        {
            get { return BorderSize; }
            set
            {
                if (value == BorderSize) return;
                BorderSize = value;
                Invalidate();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Hovering = true;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            Hovering = false;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) Pressed = true;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) Pressed = false;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var g = pe.Graphics;
            var r = ClientRectangle;
            r.Width -= 1; r.Height -= 1;

            g.FillRectangle(new SolidBrush(Parent.BackColor), ClientRectangle);

            Color fill;

            if (Pressed) fill = Color.Orange;
            else if (Hovering) fill = Color.LightCyan;
            else fill = Color.Cyan;

            if (Shape == ShapeType.Oval)
            {
                g.FillEllipse(new SolidBrush(fill), r);
                g.DrawEllipse(new Pen(Color.Blue, BorderSize), r);
            }
            else
            {
                g.FillRectangle(new SolidBrush(fill), r);
                g.DrawRectangle(new Pen(Color.Blue, BorderSize), r);
            }
            var f = new Font("Courier New", (float)r.Height * 0.3f, FontStyle.Bold, GraphicsUnit.Pixel);
            var sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            g.DrawString(Text, f, new SolidBrush(Color.Black),
                         new RectangleF((float)r.Left, (float)r.Top, (float)r.Width, (float)r.Height), sf);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {

        }
    }
}
