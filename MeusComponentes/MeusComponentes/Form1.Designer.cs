﻿namespace MeusComponentes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.meuBotao1 = new MeusComponentes.MeuBotao();
            this.meuBotao2 = new MeusComponentes.MeuBotao();
            this.SuspendLayout();
            // 
            // meuBotao1
            // 
            this.meuBotao1.Location = new System.Drawing.Point(47, 79);
            this.meuBotao1.Name = "meuBotao1";
            this.meuBotao1.Size = new System.Drawing.Size(360, 109);
            this.meuBotao1.TabIndex = 0;
            this.meuBotao1.Text = "meuBotao1";
            // 
            // meuBotao2
            // 
            this.meuBotao2.Location = new System.Drawing.Point(74, 241);
            this.meuBotao2.Name = "meuBotao2";
            this.meuBotao2.Size = new System.Drawing.Size(270, 113);
            this.meuBotao2.TabIndex = 1;
            this.meuBotao2.Text = "meuBotao2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 392);
            this.Controls.Add(this.meuBotao2);
            this.Controls.Add(this.meuBotao1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MeuBotao meuBotao1;
        private MeuBotao meuBotao2;
    }
}

