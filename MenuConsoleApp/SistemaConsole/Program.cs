﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu("MENU");
            menu.Centralizado = false;
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 1" });
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 2" });
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 3" });

			menu.Items[0].ItemSelected += Opcao1;
			menu.Items[1].ItemSelected += Opcao2;
			menu.Items[2].ItemSelected += Opcao3;
            menu.Show();
            Console.ReadKey();  
       }

	   private static void Opcao3(object sender, EventArgs e)
	   {
	   	   Console.Clear();
		   Console.WriteLine(3333333333333333);

	   }

	   private static void Opcao2(object sender, EventArgs e)
	   {
	   	   Console.Clear();
		   Console.WriteLine (222222222222222);
	   }

	   private static void Opcao1(object sender, EventArgs e)
	   {
	   	   Console.Clear();
		//   Console.WriteLine(1111111111111111);
            Menu menu = new Menu("Menu 1");
            menu.Centralizado = true;
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 1" });
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 2" });
            menu.Items.Add(new MenuItem { Rotulo = "Opcao 3" });

            menu.Items[0].ItemSelected += Opcao1;
            menu.Items[1].ItemSelected += Opcao2;
            menu.Items[2].ItemSelected += Opcao3;

            menu.Show();
            Console.ReadKey();


        }
    }
}
