﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldPD25S
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Declaração de variáveis e constantes de forma explicita"

            // numericos

            int codigo = 0;
            Int64 codigo64 = 10;

            float Percentual = 0;
            Single PercSingle = 0; // simples precisão

            double Temperatura = 10;
            Double TempDouble = 0;

            decimal Salario = 0;
            Decimal SalarioDec = 0;

            // Tipo logico

            bool Logico = false;
            Boolean LogicoBool = false;

            // Tipo string
            string Nome = "Fulano";
            String NomeStr = "Fulano";

            char caracter = 'j';
            Char caracterChar = 'j';

            //Vetores e matrizes
            int[] codigos = { 10, 20, 30, 40, 50 }; //1D
            int[,] matriz =  { { 10, 10 }, { 20, 20 } }; //2D

            codigos.Sum();

            //Coleções
            List<int> listaCodigos = new List<int>();
            HashSet<int> listaHash = new HashSet<int>();

            listaCodigos.Sort(); //ordenação dos elementos da lista

            const int CONSTANTE = 100;

            #endregion

            // declarações implicitas 

            var n = 10;
            var s = "String";

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;

            Console.Clear();

            Console.Write("Digite o código: ");
            s = Console.ReadLine();
            try
            {
                int cod = Convert.ToInt32(s);
            }
            catch (Exception e)
            {
                Console.WriteLine("Código inválido!");
                Console.WriteLine(e.Message);
            }

            Console.SetCursorPosition(15, 22);
            Console.WriteLine("Pressione qualquer tecla para encerrar");
            Console.ReadKey();

            var pf = new PessoaFisica();

            // comandos de repetição

            for(int i =0; i < codigos.Length; i++)
            {
                Console.WriteLine(codigos[i]);
            }
            
            foreach(var i in codigos)
            {
                Console.WriteLine(i);
            }

            while(true)
            {

            }

            
        }
    }
}
