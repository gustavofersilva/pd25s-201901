﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldPD25S
{
    //modificadores de acesso public, internal, private
    //modificadores de tipo (static, protected)
    public class PessoaFisica
    {
        public int Codigo { get; set; }

        private string mCPF;

        public string CPF
        {
            get { return mCPF; }
            set { CPF = value; }
        }

        private bool ValidaCPF()
        {
            if(CPF.Equals("00000000000"))
            {
                return false;
            } else
            {
                return true;
            }
        }
    }
}
