﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevisaoRAD
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {

            new FormCliente().ShowDialog(); 
        
        }

        private void btnVendedores_Click(object sender, EventArgs e)
        {
            new FormVendedores().ShowDialog();
        }

        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new frmPrincipal());
        }
    }
}
