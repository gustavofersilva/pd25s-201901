﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevisaoRAD
{
    public partial class FormVendedores : Form
    {
        public FormVendedores()
        {
            InitializeComponent();
        }

        private void vendedoresBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.vendedoresBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.clientesDataSet);

        }

        private void vendedoresBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.vendedoresBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.clientesDataSet);

        }

        private void FormVendedores_Load(object sender, EventArgs e)
        {
            // TODO: esta linha de código carrega dados na tabela 'clientesDataSet.Vendedores'. Você pode movê-la ou removê-la conforme necessário.
            this.vendedoresTableAdapter.Fill(this.clientesDataSet.Vendedores);

        }
    }
}
